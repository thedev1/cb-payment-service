using System;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Types;
using Xunit;

namespace ClearBank.DeveloperTest.Tests.Services
{
    public class PaymentValidatorTest
    {
        [Fact]
        public void FastPaymentValidation_WithInsufficientBalance_Fails()
        {
            var request = new MakePaymentRequest { Amount = 99 };
            var account = new Account()
            {
                Balance = 0, // Balance 0
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments
            };

            var actual = new PaymentValidator(request, account).IsValid();

            Assert.False(actual);
        }

        [Fact]
        public void FastPaymentValidation_WithoutAllowedScheme_Fails()
        {
            var request = new MakePaymentRequest { Amount = 99 };
            var account = new Account()
            {
                Balance = 100,
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs // FastPayments not allowed
            };

            var actual = new PaymentValidator(request, account).IsValid();

            Assert.False(actual);
        }
        
        [Fact]
        public void FastPaymentValidation_WithSufficientBalance_IsSuccessful()
        {
            var request = new MakePaymentRequest { Amount = 99 };
            var account = new Account()
            {
                Balance = 100,
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments
            };

            var actual = new PaymentValidator(request, account).IsValid();

            Assert.True(actual);
        }

    }
}
