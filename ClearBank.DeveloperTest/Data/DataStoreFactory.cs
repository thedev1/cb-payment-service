using ClearBank.DeveloperTest.Data;
using System.Configuration;

namespace ClearBank.DeveloperTest.Services
{
    public class DataStoreFactory {
        public IAccountDataStore Create (){
            var dataStoreType = ConfigurationManager.AppSettings["DataStoreType"];
            if (dataStoreType == "Backup")
            {
                return new BackupAccountDataStore();
            }
            return new AccountDataStore();
        }
    }
}
