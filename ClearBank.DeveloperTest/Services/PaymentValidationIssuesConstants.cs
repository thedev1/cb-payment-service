namespace ClearBank.DeveloperTest.Services
{
    static class PaymentValidationIssuesConstants
    {
            public const string ACCOUNT_NOT_FOUND = "Account not found";
            public const string ACCOUNT_NOT_LIVE = "Account not live";
            public const string METHOD_NOT_ALLOWED = "Method not allowed";
            public const string INSUFFICIENT_FUNDS = "Insufficient funds";
    }
}
