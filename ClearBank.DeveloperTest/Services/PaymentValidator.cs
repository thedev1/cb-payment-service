using System;
using System.Collections.Generic;
using System.Linq;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentValidator
    {
        private Dictionary<PaymentScheme, Action<MakePaymentRequest, Account>> validationMapping { get; set; }
        private IList<string> Issues = new List<string>();

        public PaymentValidator(MakePaymentRequest request, Account account)
        {
             validationMapping = new Dictionary<PaymentScheme, Action<MakePaymentRequest, Account>>()
            {
                 { PaymentScheme.FasterPayments, ValidateFasterPayments },
                 { PaymentScheme.Bacs, ValidateBacs },
                 { PaymentScheme.Chaps, ValidateChaps }
            };

            validationMapping[request.PaymentScheme](request, account);
        }

        public bool IsValid()
        {
            return !Issues.Any();
        }

        private void ValidateChaps(MakePaymentRequest request, Account account)
        {
            if (account == null)
            {
                Issues.Add(PaymentValidationIssuesConstants.ACCOUNT_NOT_FOUND);
            }
            else if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
            {
                Issues.Add(PaymentValidationIssuesConstants.METHOD_NOT_ALLOWED);
            }
            else if (account.Status != AccountStatus.Live)
            {
                Issues.Add(PaymentValidationIssuesConstants.ACCOUNT_NOT_LIVE);
            }
        }

        private void ValidateBacs(MakePaymentRequest request, Account account)
        {
            if (account == null)
            {
                Issues.Add(PaymentValidationIssuesConstants.ACCOUNT_NOT_FOUND);
            }
            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
            {
                Issues.Add(PaymentValidationIssuesConstants.METHOD_NOT_ALLOWED);
            }
        }

        private void ValidateFasterPayments(MakePaymentRequest request, Account account)
        {
            if (account == null)
            {
                Issues.Add(PaymentValidationIssuesConstants.ACCOUNT_NOT_FOUND);
            }
            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
            {
                Issues.Add(PaymentValidationIssuesConstants.METHOD_NOT_ALLOWED);
            }
             if (account.Balance < request.Amount)
            {
                Issues.Add(PaymentValidationIssuesConstants.INSUFFICIENT_FUNDS);
            }
        }
    }
    }
