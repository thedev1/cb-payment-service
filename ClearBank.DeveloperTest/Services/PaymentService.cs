﻿using System;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private static readonly IAccountDataStore DataStore = new DataStoreFactory().Create();
        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var result = new MakePaymentResult();
            try
            {
                var account = DataStore.GetAccount(request.DebtorAccountNumber);
                ValidatePayment(request, account);
                HandlePayment(request, account);
                result.Success = true;
            }
            catch (Exception)
            {
                result.Success = false;
            }
            return result;
        }
        private void ValidatePayment(MakePaymentRequest request, Account account)
        {
            var paymentValidator = new PaymentValidator(request, account);
            if (paymentValidator.IsValid())
                return;

            throw new Exception("💣"); // get more details from validator Errors collection
        }
        private void HandlePayment(MakePaymentRequest request, Account account)
        {
            account.Balance -= request.Amount;
            DataStore.UpdateAccount(account);
            return;
        }
    }
}